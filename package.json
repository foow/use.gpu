{
  "name": "use.gpu",
  "version": "0.9.1",
  "main": "index.js",
  "author": "Steven Wittens <steven@acko.net>",
  "license": "MIT",
  "private": true,
  "dependencies": {
    "react": "^18",
    "react-dom": "^18"
  },
  "devDependencies": {
    "@babel/cli": "^7.18.6",
    "@babel/core": "^7.18.6",
    "@babel/eslint-parser": "^7.18.2",
    "@babel/plugin-transform-modules-commonjs": "^7.18.6",
    "@babel/preset-env": "^7.18.6",
    "@babel/preset-typescript": "^7.18.6",
    "@babel/register": "^7.18.6",
    "@types/jest": "^26.0.19",
    "@types/lodash": "^4.14.176",
    "@types/lru-cache": "^5.1.1",
    "@types/webpack": "^5.10.0",
    "@typescript-eslint/eslint-plugin": "^4.22.0",
    "@typescript-eslint/parser": "^4.22.0",
    "@wasm-tool/wasm-pack-plugin": "^1.6.0",
    "@webgpu/types": "^0.1.23",
    "babel-jest": "^26.6.3",
    "babel-loader": "^8.2.2",
    "css-loader": "^6.0.0",
    "dotenv": "^16.0.1",
    "eslint": "^7.5.0",
    "getdocs-ts": "^0.1.2",
    "jest": "^27.2.0",
    "magic-string": "^0.26.2",
    "node-fetch": "^3.2.10",
    "raw-loader": "^4.0.2",
    "react-refresh": "^0.14.0",
    "rollup-pluginutils": "^2.8.2",
    "style-loader": "^3.3.1",
    "terser-webpack-plugin": "^5.3.5",
    "ts-jest": "^27.0.0",
    "ts-node": "^10.9.1",
    "typescript": "^4.8.0-dev.20220709",
    "webpack": "^5.73.0",
    "webpack-cli": "^4.10.0",
    "webpack-dev-server": "^3.11.0",
    "webpack-dotenv-plugin": "^2.1.0",
    "wsrun": "^5.2.4"
  },
  "ext": {
    "typescript": "^4.7.0-dev.20220506"
  },
  "browserslist": [
    "last 1 chrome version",
    "last 1 firefox version"
  ],
  "scripts": {
    "build+ci": "yarn run build && yarn run ci-test",
    "build": "yarn wsrun --stages run build && yarn run docs",
    "build-app": "ts-node node_modules/.bin/webpack build; ./scripts/copy-app-to-docs.sh",
    "clear": "rm -rf packages/glyph/pkg rust/use-gpu-text/pkg packages/shader/glsl/grammar/glsl.js packages/shader/glsl/grammar/glsl.terms.js packages/shader/wgsl/grammar/wgsl.js packages/shader/wgsl/grammar/wgsl.terms.js",
    "ci-test": "CI=true yarn run jest && yarn run test:import && yarn run test:vite-react",
    "docs": "ts-node ./scripts/getdocs.ts",
    "publish": "./scripts/npm-publish.sh",
    "start": "NODE_ENV=development ts-node node_modules/.bin/webpack serve",
    "test": "MODULE_ENV=cjs jest --watch",
    "test:import": "(cd test/import; yarn run test)",
    "lint": "eslint packages/**/src --ext ts",
    "types": "yarn wsrun run types",
    "coverage": "yarn wsrun run coverage",
    "postinstall": "(cd packages/wgsl; yarn run build); (cd packages/glyph; yarn run build:wasm); (cp -n .env.local.example .env.local || true)"
  },
  "jest": {
    "preset": "ts-jest",
    "testEnvironment": "jsdom",
    "transform": {
      "\\.[jt]sx?$": "babel-jest",
      "\\.(wgsl|glsl)$": "<rootDir>/test/fileTransformer.mjs"
    },
    "moduleNameMapper": {
      "^@use-gpu/wgsl/(.*)$": "<rootDir>/packages/wgsl/src/$1",
      "^@use-gpu/(.*)$": "<rootDir>/packages/$1"
    },
    "modulePaths": [
      "<rootDir>/packages"
    ],
    "modulePathIgnorePatterns": [
      "<rootDir>/build"
    ],
    "setupFiles": [
      "./test/setup.ts"
    ],
    "moduleFileExtensions": [
      "js",
      "jsx",
      "ts",
      "tsx",
      "glsl",
      "wgsl"
    ],
    "globals": {
      "ts-jest": {
        "diagnostics": {
          "ignoreCodes": [
            151001
          ]
        }
      },
      "GPUShaderStage": {
        "VERTEX": 1,
        "FRAGMENT": 2,
        "COMPUTE": 4
      }
    }
  },
  "workspaces": [
    "packages/state",
    "packages/wgsl",
    "packages/live",
    "packages/traits",
    "packages/core",
    "packages/shader",
    "packages/glsl-loader",
    "packages/wgsl-loader",
    "packages/react",
    "packages/glyph",
    "packages/workbench",
    "packages/scene",
    "packages/webgpu",
    "packages/gltf",
    "packages/map",
    "packages/layout",
    "packages/plot",
    "packages/present",
    "packages/voxel",
    "packages/inspect",
    "packages/inspect-gpu",
    "packages/app"
  ]
}
