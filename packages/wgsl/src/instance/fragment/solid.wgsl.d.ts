declare module "@use-gpu/wgsl/instance/fragment/solid.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSolidFragment: ParsedBundle;
  export default __module;
}
