declare module "@use-gpu/wgsl/instance/fragment/light.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLightFragment: ParsedBundle;
  export default __module;
}
