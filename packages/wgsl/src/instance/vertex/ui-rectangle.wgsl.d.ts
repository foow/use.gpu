declare module "@use-gpu/wgsl/instance/vertex/ui-rectangle.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getUIRectangleVertex: ParsedBundle;
  export default __module;
}
