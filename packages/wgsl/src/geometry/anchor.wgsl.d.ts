declare module "@use-gpu/wgsl/geometry/anchor.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getLineAnchor: ParsedBundle;
  export default __module;
}
